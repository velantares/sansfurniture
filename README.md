An unfinished collecting game, to be updated someday.
    Copyright (C) 2020  Jemillee Prado

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 - I chose this license because it is a free software/game. The files can be
   seen and studied. It is a good thing to share what you know with people.

# TO INSTALL:

1. Download the repository
2. Unzip the files
3. Open 'sansfurniture' folder
4. Open 'JPFinalProject' folder
5. Open 'publish' folder
6. Open 'setup' to start downloading.
   Your computer can't verify the publisher, so install it anyway.
   (I don't know how to make it verirified.)
7. The game will automatically start after download.

The unfinished game will be visible on your "Recently added"
if you click the Windows button. Or just visit the Start menu.