﻿/*
 * CollisionCoin.cs
 * Description: controls reactions to coin collisions
 *      checks if collision happened.
 *      
 * Revision History: 
 *      Jemillee Prado 12.03.2019: Created
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    public class CollisionCoin : GameComponent
    {
        private Coin coin;
        private Hero hero;
        private PlayScene gamePlayScene;
        private SoundEffect coinSound;

        public CollisionCoin(Game game,
            Hero hero,
            Coin coin,
            PlayScene gamePlayScene) : base(game)
        {
            Game1 g = (Game1)game;
            this.gamePlayScene = gamePlayScene;
            this.hero = hero;
            this.coin = coin;
            coinSound = g.Content.Load<SoundEffect>("Music/GetCoin");
        }

        public override void Update(GameTime gameTime)
        {
            Rectangle coinRect = coin.getBound();
            Rectangle heroRect = hero.getBound();

            //COIN
            if (heroRect.Intersects(coinRect))
            {
                if (coin.Enabled == true)
                {
                    coinSound.Play();
                    gamePlayScene.score++;
                    coin.Enabled = false;
                    coin.Visible = false;
                }
                else
                {
                    //do nothing.
                }
            }            

            base.Update(gameTime);
        }

    }
}
