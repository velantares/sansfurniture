﻿/*
 * CollisionDoor.cs
 * Description: controls reactions to door collisions
 *      checks if collision happened.
 *      
 * Revision History: 
 *      Jemillee Prado 12.03.2019: Created
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    public class CollisionDoor: DrawableGameComponent
    {
        private Door door;
        private Hero hero;
        private PlayScene gamePlayScene;

        public CollisionDoor(Game game,
            Hero hero,
            Door door,
            PlayScene gamePlayScene) : base(game)
        {
            this.gamePlayScene = gamePlayScene;
            this.hero = hero;
            this.door = door;
        }

        public override void Update(GameTime gameTime)
        {
            Rectangle doorRect = door.getBound();
            Rectangle heroRect = hero.getBound();

            //COIN
            if (heroRect.Intersects(doorRect))
            {
                if (gamePlayScene.isKeyTaken == true)
                {
                    gamePlayScene.isDoorOpen = true;
                }
                else
                {
                    //do nothing.
                }
            }

            base.Update(gameTime);
        }
    }
}
