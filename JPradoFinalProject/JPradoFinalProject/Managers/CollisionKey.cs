﻿/*
 * CollisionKeycs
 * Description: controls reactions to key collisions
 *      checks if collision happened.
 *      
 * Revision History: 
 *      Jemillee Prado 12.08.2019: Created
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    class CollisionKey: DrawableGameComponent
    {
        private Key key;
        private Hero hero;
        private PlayScene gamePlayScene;

        public CollisionKey(Game game,
            Hero hero,
            Key key,
            PlayScene gamePlayScene) : base(game)
        {
            this.gamePlayScene = gamePlayScene;
            this.hero = hero;
            this.key = key;
        }

        public override void Update(GameTime gameTime)
        {
            Rectangle keyRect = key.getBound();
            Rectangle heroRect = hero.getBound();

            //COIN
            if (heroRect.Intersects(keyRect))
            {
                if (key.Enabled == true)
                {
                    gamePlayScene.score = gamePlayScene.score + 5;
                    gamePlayScene.isKeyTaken = true;
                    key.Enabled = false;
                    key.Visible = false;
                }
                else
                {
                    //do nothing.
                }
            }

            base.Update(gameTime);
        }

    }
}
