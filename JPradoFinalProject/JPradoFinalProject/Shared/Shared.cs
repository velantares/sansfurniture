﻿/*
 * Shared.cs
 * Description: shares static stage and fullscreen info
 *      
 * Revision History: 
 *      Jemillee Prado 12.06.2019: Created
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    public class Shared
    {
        public static Vector2 stage;
        public static Rectangle fullScreen;
    }
}
