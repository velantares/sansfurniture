﻿/*
 * ScoreString.cs 
 * Description: constructs a score for coin taken
 *      
 * Revision History: 
 *      Jemillee Prado 12.03.2019: Created
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    public class ScoreString : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private string message;
        private Vector2 position;
        private Color color;
        private SpriteFont font;

        public string Message { get => message; set => message = value; }
        public Vector2 Position { get => position; set => position = value; }
        public ScoreString(Game game,
            SpriteBatch spriteBatch,
            SpriteFont font,
            string message,
            Vector2 position) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.font = font;
            this.message = message;
            this.position = position;
            color = Color.White;
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(font, Message, Position, color);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
