﻿/*
 * ScoreBox.cs
 * Description: box for the score
 *      
 * Revision History: 
 *      Jemillee Prado 12.06.2019: Created
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    public class ScoreBox : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Texture2D tex;
        private Rectangle size;

        public ScoreBox(Game game,
            SpriteBatch spriteBatch,
            Texture2D tex,
            Rectangle size) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.tex = tex;
            this.size = size;
        }
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(tex, size, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }

    }
}
