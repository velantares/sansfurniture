﻿/*
 * Hero.cs
 * Description: constructs and updates hero when
 *      hero is moved.
 *      
 * Revision History: 
 *      Jemillee Prado 12.03.2019: Created
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    public class Hero : DrawableGameComponent
    {
        //declare properties of the character
        private SpriteBatch spriteBatch;
        private Texture2D tex;
        private Texture2D upWalk;
        private Texture2D downWalk;
        private Texture2D leftWalk;
        private Texture2D rightWalk;
        private Vector2 position;
        private Vector2 dimension;
        private List<Rectangle> frames; // to animate
        private int frameIndex = 0;
        private int delay; //specify delay between 2 frames, speed
        private int delayCounter;
        private Vector2 speed = new Vector2(6, 6); //walk
        private Vector2 stage; //wall

        public Texture2D Tex { get => tex; set => tex = value; }
        public Vector2 Position { get => position; set => position = value; }
        public Vector2 Stage { get => stage; set => stage = value; }

        // rows and columns
        private const int ROW = 1;
        private const int COL = 4;

        //start for walk animation
        public void start()
        {
            Visible = true;
            Enabled = true;
        }

        public Hero(Game game,
            SpriteBatch spriteBatch,
            Texture2D tex,
            Vector2 position,
            Vector2 stage,
            int delay) : base(game)
        {
            Game1 g = (Game1)game;
            this.spriteBatch = spriteBatch;
            this.tex = tex;
            this.position = position;
            this.stage = stage;
            this.delay = delay;
            dimension = new Vector2(tex.Width / COL, tex.Height / ROW);
            upWalk = g.Content.Load<Texture2D>("Images/HeroUp");
            downWalk = g.Content.Load<Texture2D>("Images/HeroDown");
            leftWalk = g.Content.Load<Texture2D>("Images/HeroLeft");
            rightWalk = g.Content.Load<Texture2D>("Images/HeroRight");
            start();

            createFrames();
        }

        private void createFrames()
        {
            frames = new List<Rectangle>(); //to be added to this list
            for (int i = 0; i < ROW; i++)
            {
                for (int j = 0; j < COL; j++)
                {
                    int x = j * (int)dimension.X;
                    int y = i * (int)dimension.Y;
                    Rectangle r = new Rectangle(x, y, (int)dimension.X, (int)dimension.Y); //vector is float
                    frames.Add(r);
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            if (frameIndex >= 0)    //then DRAW from UPDATE
            {
                spriteBatch.Draw(tex, position, frames[frameIndex], Color.White);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            //no diagonals.

            //up
            if (ks.IsKeyDown(Keys.Up) || ks.IsKeyDown(Keys.W))
            {
                tex = upWalk;
                position.Y -= speed.Y;
                delayCounter++; //init 0
                if (delayCounter > delay)
                {
                    frameIndex++; // -1 init becomes zero

                    // if more than 24
                    if (frameIndex > ROW * COL - 1)
                    {
                        frameIndex = 0;
                    }

                    //will be back
                    delayCounter = 0;
                }
            }
            //down
            if (ks.IsKeyDown(Keys.Down) || ks.IsKeyDown(Keys.S))
            {
                tex = downWalk;
                position.Y += speed.Y;
                delayCounter++; //init 0
                if (delayCounter > delay)
                {
                    frameIndex++; // -1 init becomes zero

                    // if more than 24
                    if (frameIndex > ROW * COL - 1)
                    {
                        frameIndex = 0;
                    }

                    //will be back
                    delayCounter = 0;
                }
            }
            //left
            if (ks.IsKeyDown(Keys.Left) || ks.IsKeyDown(Keys.A))
            {
                tex = leftWalk;
                position.X -= speed.X;
                delayCounter++; //init 0
                if (delayCounter > delay)
                {
                    frameIndex++; // -1 init becomes zero

                    // if more than 24
                    if (frameIndex > ROW * COL - 1)
                    {
                        frameIndex = 0;
                    }

                    //will be back
                    delayCounter = 0;
                }
            }
            //right
            if (ks.IsKeyDown(Keys.Right) || ks.IsKeyDown(Keys.D))
            {
                tex = rightWalk;
                position.X += speed.X;
                delayCounter++; //init 0
                if (delayCounter > delay)
                {
                    frameIndex++; // -1 init becomes zero

                    // if more than 24
                    if (frameIndex > ROW * COL - 1)
                    {
                        frameIndex = 0;
                    }

                    //will be back
                    delayCounter = 0;
                }
            }
            //opposite keys
            if (((ks.IsKeyDown(Keys.S) || ks.IsKeyDown(Keys.Down)) &&
                (ks.IsKeyDown(Keys.W) || ks.IsKeyDown(Keys.Up))) ||
                ((ks.IsKeyDown(Keys.D) || ks.IsKeyDown(Keys.Right)) &&
                (ks.IsKeyDown(Keys.A) || ks.IsKeyDown(Keys.Left))))
            {
                tex = downWalk;
            }

            //window wall barrier
            //up
            if (position.Y < 0)
            {
                position.Y = 0; //because 0 is wall.
            }
            //left
            if (position.X < 0)
            {
                position.X = 0; //because 0 is wall.
            }
            //down... works.
            if (position.Y + tex.Height > stage.Y)
            {
                position.Y = stage.Y - tex.Height;
            }
            //right
            if (position.X + tex.Width / COL > stage.X)
            {
                //stage - width of tex /4 because animation. 
                position.X = stage.X - tex.Width / COL;
            }

            base.Update(gameTime);
        }

        public Rectangle getBound()
        {
            return new Rectangle((int)position.X, (int)position.Y, tex.Width / COL, tex.Height);
        }
    }
}
