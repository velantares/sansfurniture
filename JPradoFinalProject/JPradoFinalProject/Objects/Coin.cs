﻿/*
 * Coin.cs
 * Description: coin information and 
 *      construction and animation
 *          
 * Revision History: 
 *      Jemillee Prado 12.03.2019: Created
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    public class Coin : DrawableGameComponent
    {
        //declare properties of coin
        private SpriteBatch spriteBatch;
        private Texture2D tex;
        private Vector2 position;
        private Vector2 dimension;
        private List<Rectangle> frames; // to animate
        private int frameIndex = 0;
        private int delay; //specify delay between 2 frames, speed
        private int delayCounter;
        private Vector2 stage;

        private const int COL = 6;
        private const int ROW = 1;
        public Coin(Game game,
            SpriteBatch spriteBatch,
            Texture2D tex,
            Vector2 position,
            Vector2 stage,
            int delay) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.tex = tex;
            this.position = position;
            this.stage = stage;
            this.delay = delay;
            dimension = new Vector2(tex.Width / COL, tex.Height / ROW);
            start();

            createFrames();
        }

        private void start()
        {
            Enabled = true;
            Visible = true;
        }


        private void createFrames()
        {
            frames = new List<Rectangle>(); //to be added to this list
            for (int i = 0; i < ROW; i++)
            {
                for (int j = 0; j < COL; j++)
                {
                    int x = j * (int)dimension.X;
                    int y = i * (int)dimension.Y;
                    Rectangle r = new Rectangle(x, y, (int)dimension.X, (int)dimension.Y); //vector is float
                    frames.Add(r);
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            if (frameIndex >= 0)    //then DRAW from UPDATE
            {
                spriteBatch.Draw(tex, position, frames[frameIndex], Color.White);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            //begin the coin turns
            delayCounter++; //init 0
            if (delayCounter > delay)
            {
                frameIndex++; // -1 init becomes zero

                // if more than 24
                if (frameIndex > ROW * COL - 1)
                {
                    frameIndex = 0;
                }

                //will be back
                delayCounter = 0;
            }
            base.Update(gameTime);
        }

        public Rectangle getBound()
        {
            return new Rectangle((int)position.X, (int)position.Y, tex.Width / COL, tex.Height);

        }

    }
}
