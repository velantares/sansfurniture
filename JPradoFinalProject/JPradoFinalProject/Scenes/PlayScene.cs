﻿/*
 * PlayScene.cs
 * Description: the main action play scene of the game
 *      
 * Revision History: 
 *      Jemillee Prado 12.03.2019: Created (GameForm)
 *      Jemillee Prado 12.06.2019: Transfered GameForm
 *          to PlayScene)       
 */

using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    public class PlayScene : GameScene
    {
        private SpriteBatch spriteBatch;
        private ScoreString scoreString;
        public int score;
        public bool isKeyTaken;
        public bool isDoorOpen;
        public bool isScoreGone;
        private ScoreBox scoreBox;
        private ScoreString endString;

        //declare Collisions and Objects
        private Hero hero;
        private Coin[] coin;
        private CollisionCoin cc;
        private CollisionKey ck;
        private CollisionDoor cd;
        private Floor[,] floor;
        private Door door;
        private Key key;


        public PlayScene(Game game) : base(game)
        {
            Game1 g = (Game1)game;

            this.spriteBatch = g.spriteBatch;

            //FLOOR TEST
            Vector2 fPos = new Vector2(0, 0);
            Texture2D fTex = g.Content.Load<Texture2D>("Images/FloorBackground");

            floor = new Floor[5, 5];
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    floor[i, j] = new Floor(g, spriteBatch, fTex, fPos);
                    this.Components.Add(floor[i, j]);
                    fPos.X += fTex.Width;
                }
                fPos.Y += fTex.Height; //column separation

                fPos.X = 0;
            }

            //DOOR
            Texture2D doorTex = g.Content.Load<Texture2D>("Images/DoorSingle");
            Vector2 doorPos = new Vector2(Shared.stage.X - doorTex.Width, Shared.stage.Y / 2);
            door = new Door(g, spriteBatch, doorTex, doorPos);
            this.Components.Add(door);

            //KEY
            Texture2D keyTex = g.Content.Load<Texture2D>("Images/Key");
            Vector2 keyPos = new Vector2(Shared.stage.X / 2, Shared.stage.Y / 2);
            key = new Key(g, spriteBatch, keyTex, keyPos);
            this.Components.Add(key);

            //HERO
            Texture2D tex = g.Content.Load<Texture2D>("Images/HeroDown");
            Vector2 cPos = new Vector2(0, 0);
            Vector2 stage = Shared.stage;

            hero = new Hero(g, spriteBatch, tex, cPos, stage, 7);
            this.Components.Add(hero);

            //COIN
            Texture2D coinTex = g.Content.Load<Texture2D>("Images/coin");

            coin = new Coin[10];
            Random r = new Random();
            for (int i = 0; i < coin.Length; i++)
            {
                coin[i] = new Coin(g, spriteBatch, coinTex,
                    new Vector2(r.Next(0, (int)stage.X - coinTex.Width),
                    r.Next(0, (int)stage.Y - coinTex.Height)),
                    stage, 5);
                this.Components.Add(coin[i]);
            }

            //SCORE
            SpriteFont font = g.Content.Load<SpriteFont>("MyFonts/RegularFont");
            string message = "Score: ";
            scoreString = new ScoreString(g, spriteBatch, font, message, new Vector2(10, 10));
            this.Components.Add(scoreString);

            //COLLISIONS - COINS
            for (int i = 0; i < coin.Length; i++)
            {
                cc = new CollisionCoin(g, hero, coin[i], this);
                this.Components.Add(cc);
            }

            //COLLISIONS - KEY
            ck = new CollisionKey(g, hero, key, this);
            this.Components.Add(ck);

            //COLLISIONS - DOOR
            cd = new CollisionDoor(g, hero, door, this);
            this.Components.Add(cd);

            //SCOREBOX
            SpriteFont sFont = g.Content.Load<SpriteFont>("MyFonts/ScoreFont");
            Rectangle sPos = Shared.fullScreen;
            Texture2D sTex = g.Content.Load<Texture2D>("Images/Score");
            scoreBox = new ScoreBox(g, spriteBatch, sTex, sPos);
            this.Components.Add(scoreBox);


            SpriteFont eFont = g.Content.Load<SpriteFont>("MyFonts/ScoreFont");
            string eMessage = "Your score is: " + score +
                "\n\nPress the spacebar to return to menu screen.";
            endString = new ScoreString(g, spriteBatch, eFont, eMessage, new Vector2(10, 10));
            this.Components.Add(endString);

            scoreBox.Visible = false;
            endString.Visible = false;
            isScoreGone = false;
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();

            string message = "Score: " + score;
            scoreString.Message = message;

            string eMessage = "Your score is: " + score +
                "\n\nPress the spacebar to return to menu screen.";
            endString.Message = eMessage;

            if (isDoorOpen == true)
            {
                if(isScoreGone == false)
                {
                    scoreBox.Visible = true;
                    endString.Visible = true;
                }  
            }

            if(scoreBox.Visible == true)
            {
                if (ks.IsKeyDown(Keys.Space))
                {                    
                    isScoreGone = true;
                }
            }
            base.Update(gameTime);
        }
    }
}
