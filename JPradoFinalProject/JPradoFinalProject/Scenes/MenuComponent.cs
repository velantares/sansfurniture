﻿/*
 * MenuComponent.cs
 * Description: makes the menu select items
 *      
 * Revision History: 
 *      Jemillee Prado 12.06.2019: Created
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    public class MenuComponent : DrawableGameComponent
    {
        //declare fields
        private SpriteBatch spriteBatch;
        private SpriteFont regularFont, selectedFont;
        private List<string> menuItems;
        private int selectedIndex = 0;
        private Vector2 position;
        private Color mainColor = Color.Black;
        private Color selectedColor = Color.White;

        private KeyboardState oldState; //for old keyboardstate changes

        //selectedindex property for selection use
        public int SelectedIndex { get => selectedIndex; set => selectedIndex = value; }

        //generate menu
        public MenuComponent(Game game,
            SpriteBatch spriteBatch,
            SpriteFont regularFont,
            SpriteFont selectedFont,
            string[] menus) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.regularFont = regularFont;
            this.selectedFont = selectedFont;
            menuItems = menus.ToList(); //make list
            position = new Vector2(Shared.stage.X / 2 - 40, Shared.stage.Y / 2 - 20);
        }

        //draw menu comp
        public override void Draw(GameTime gameTime)
        {
            Vector2 tempPos = position;
            spriteBatch.Begin();
            for (int i = 0; i < menuItems.Count; i++)
            {
                if (selectedIndex == i)
                {
                    spriteBatch.DrawString(selectedFont, menuItems[i],
                        tempPos, selectedColor);
                    tempPos.Y += selectedFont.LineSpacing;
                }
                else
                {
                    spriteBatch.DrawString(regularFont, menuItems[i],
                        tempPos, mainColor);
                    tempPos.Y += regularFont.LineSpacing;
                }
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        //update menucomp
        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Down) && oldState.IsKeyUp(Keys.Down))
            {
                selectedIndex++; //goes down list
                if (selectedIndex == menuItems.Count)
                {
                    selectedIndex = 0;
                }
            }
            if (ks.IsKeyDown(Keys.Up) && oldState.IsKeyUp(Keys.Up))
            {
                selectedIndex--; //goes up list
                if (selectedIndex == -1)
                {
                    //so it does not go beyond
                    selectedIndex = menuItems.Count - 1;
                }
            }
            oldState = ks; // to make ks the new oldState

            base.Update(gameTime);
        }
    }
}
