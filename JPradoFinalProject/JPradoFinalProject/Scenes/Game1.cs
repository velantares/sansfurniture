﻿/*
 * Game1.cs
 * Description: main game controller that
 *      allows switching of scenes
 *      
 * Revision History: 
 *      Jemillee Prado 12.06.2019: Created
 */

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio; //songs
using Microsoft.Xna.Framework.Media;

namespace JPradoFinalProject
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;

        //declare all scenes here
        private StartScene startScene;
        //gamestart scene
        private PlayScene gamePlayScene;
        //tutorial
        private TutorialScene tutorialScene;
        //constructionscene
        private ConstructionScene constructionScene;
        //about
        private AboutScene aboutScene;

        public int gscore;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            Shared.stage = new Vector2(graphics.PreferredBackBufferWidth,
                graphics.PreferredBackBufferHeight);
            Shared.fullScreen = new Rectangle(0, 0,
                graphics.PreferredBackBufferWidth,
                graphics.PreferredBackBufferHeight);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            Song gameMusic = this.Content.Load<Song>("Music/GameMusic");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(gameMusic);

            //initiate start SCENE
            startScene = new StartScene(this);
            this.Components.Add(startScene);

            //create other scenes: they're hidded, so start scene must start it
            gamePlayScene = new PlayScene(this);
            this.Components.Add(gamePlayScene);

            tutorialScene = new TutorialScene(this);
            this.Components.Add(tutorialScene);

            constructionScene = new ConstructionScene(this);
            this.Components.Add(constructionScene);
            
            aboutScene = new AboutScene(this);
            this.Components.Add(aboutScene);

            //only show startscene
            startScene.show();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed 
            //|| Keyboard.GetState().IsKeyDown(Keys.Escape))
            //    Exit();

            // TODO: Add your update logic here
            int selectedIndex;
            KeyboardState ks = Keyboard.GetState();

            if (startScene.Enabled)
            {
                selectedIndex = startScene.Menu.SelectedIndex;
                if (selectedIndex == 0 && ks.IsKeyDown(Keys.Enter))
                {
                    gamePlayScene = new PlayScene(this);
                    this.Components.Add(gamePlayScene);
                    gamePlayScene.show();
                    startScene.hide();
                }
                else if (selectedIndex == 1 && ks.IsKeyDown(Keys.Enter))
                {
                    tutorialScene.show();
                    startScene.hide();
                }
                else if (selectedIndex == 2 && ks.IsKeyDown(Keys.Enter))
                {
                    constructionScene.show();
                    startScene.hide();
                }
                else if (selectedIndex == 3 && ks.IsKeyDown(Keys.Enter))
                {
                    aboutScene.show();
                    startScene.hide();
                }
                else if (selectedIndex == 4 && ks.IsKeyDown(Keys.Enter))
                {
                    Exit(); //terminate program
                }
            }

            if (gamePlayScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    startScene.show();
                    gamePlayScene.hide();
                }
            }

            if (constructionScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    startScene.show();
                    constructionScene.hide();
                }
            }

            if (tutorialScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    startScene.show();
                    tutorialScene.hide();
                }
            }

            if (aboutScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    startScene.show();
                    aboutScene.hide();
                }
            }

            if (gamePlayScene.isScoreGone == true)
            {
                startScene.show();
                gamePlayScene.hide();
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
