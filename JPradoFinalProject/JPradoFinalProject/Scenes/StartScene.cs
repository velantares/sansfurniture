﻿/*
 * StartScene.cs
 * Description: main menu of the game
 *      
 * Revision History: 
 *      Jemillee Prado 12.06.2019: Created
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    public class StartScene : GameScene
    {
        private MenuComponent menu;
        private SpriteBatch spriteBatch;
        private StartBackground background;

        public MenuComponent Menu { get => menu; set => menu = value; }

        private string[] menuItems = {"Play",
        "Help",
        "Options",
        "About",
        "Quit"};


        public StartScene(Game game) : base(game)
        {
            Game1 g = (Game1)game;
            this.spriteBatch = g.spriteBatch;

            Texture2D tex = g.Content.Load<Texture2D>("Images/SansFurniture");
            background = new StartBackground(game, spriteBatch, tex, Shared.fullScreen);
            this.Components.Add(background);

            SpriteFont mainFont = g.Content.Load<SpriteFont>("MyFonts/RegularFont");
            SpriteFont selectedFont = g.Content.Load<SpriteFont>("MyFonts/SelectedFont");

            menu = new MenuComponent(game, spriteBatch,
                mainFont, selectedFont, menuItems);
            this.Components.Add(menu);
        }
    }
}
