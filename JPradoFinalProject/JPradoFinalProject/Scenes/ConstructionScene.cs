﻿/*
 * ConstructionScene.cs (Under Construction)
 * Description: displays an "under construction" image
 *      
 * Revision History: 
 *      Jemillee Prado 12.06.2019: Created
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace JPradoFinalProject
{
    public class ConstructionScene : GameScene
    {
        private SpriteBatch spriteBatch;
        private Texture2D tex;
        private Rectangle size;

        public ConstructionScene(Game game) : base(game)
        {
            //cast to game1
            Game1 g = (Game1)game;
            this.spriteBatch = g.spriteBatch;
            tex = g.Content.Load<Texture2D>("Images/UnderConstruction");
            size = Shared.fullScreen;
        }

        public override void Draw(GameTime gameTime)
        {
            //draw construction image
            spriteBatch.Begin();
            spriteBatch.Draw(tex, size, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
