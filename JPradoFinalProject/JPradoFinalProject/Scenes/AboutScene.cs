﻿/*
 * AboutScene.cs
 * Description: shows info on about
 *      
 * Revision History: 
 *      Jemillee Prado 12.08.2019: Created
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    public class AboutScene : GameScene
    {
        private SpriteBatch spriteBatch;
        private Texture2D tex;
        private Rectangle size;

        public AboutScene(Game game) : base(game)
        {
            //cast to game1
            Game1 g = (Game1)game;
            this.spriteBatch = g.spriteBatch;
            tex = g.Content.Load<Texture2D>("Images/About");
            size = Shared.fullScreen;
        }

        public override void Draw(GameTime gameTime)
        {
            //draw about image
            spriteBatch.Begin();
            spriteBatch.Draw(tex, size, Color.White);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
