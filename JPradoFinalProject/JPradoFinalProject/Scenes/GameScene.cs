﻿/*
 * GameScene.cs
 * Description: show/hide components and items
 *      
 * Revision History: 
 *      Jemillee Prado 12.06.2019: Created
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace JPradoFinalProject
{
    public abstract class GameScene : DrawableGameComponent
    {
        //ref from allinone example
        private List<GameComponent> components;
        public List<GameComponent> Components { get => components; set => components = value; }

        public GameScene(Game game) : base(game)
        {
            //components = scenes
            components = new List<GameComponent>();
            hide();
        }
        
        public virtual void show()
        {
            Enabled = true; //interaction
            Visible = true; //visual
        }

        public virtual void hide()
        {
            Enabled = false; //no interaction
            Visible = false; //invisible
        }

        public override void Draw(GameTime gameTime)
        {
            DrawableGameComponent comp = null; //if not full
            foreach (GameComponent item in components)
            {
                if (item is DrawableGameComponent)
                {
                    comp = (DrawableGameComponent)item;
                    if (comp.Visible)
                    {
                        comp.Draw(gameTime); //draws the scene
                    }
                }
            }

            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            foreach (GameComponent item in components)
            {
                if (item.Enabled)
                {
                    item.Update(gameTime); //update if enabled
                }
            }

            base.Update(gameTime);
        }
    }
}
